<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Category;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/admin', function () {
//     return view('template.main');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/beranda', function () {
    return view('template.home');
});

// Route Products 
Route::get('/products/create', 'ProductsController@create');

Route::post('/products', 'ProductsController@store');

Route::get('/products', 'ProductsController@index');

Route::get('/products/{id}', 'ProductsController@show');

Route::get('/products/{id}/edit', 'ProductsController@edit');

Route::put('/products/{id}', 'ProductsController@update');

Route::delete('/products/{id}', 'ProductsController@destroy');

Route::resource('products', 'ProductsController');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route Categories
Route::get('/categories/create', 'CategoriesController@create');

Route::post('/categories', 'CategoriesController@store');

Route::get('/categories', 'CategoriesController@index');

Route::get('/categories/{id}', 'CategoriesController@show');

Route::get('/categories/{id}/edit', 'CategoriesController@edit');

Route::put('/categories/{id}', 'CategoriesController@update');

Route::delete('/categories/{id}', 'CategoriesController@destroy');

Route::resource('categories', 'CategoriesController');


// Route carts
Route::get('/carts/create', 'CartsController@create');

Route::post('/carts', 'CartsController@store');

Route::get('/carts', 'CartsController@index');

Route::get('/carts/{id}', 'CartsController@show');

Route::get('/carts/{id}/edit', 'CartsController@edit');

Route::put('/carts/{id}', 'CartsController@update');

Route::delete('/carts/{id}', 'CartsController@destroy');

Route::resource('carts', 'CartsController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('excel-products', 'ProductsController@export');
