<?php

namespace App\Http\Controllers;

use App\Exports\ProductsExport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\Product;

class ProductsController extends Controller
{
    // public function __construct() {
    //     $this->middleware('auth');
    // }

    public function create() {
        return view('products.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:products',
            'stok' => 'required',
            'deskripsi' => 'required',
            'image' => 'required',
            'harga' => 'required'
        ]);

        // Query Builder
        // $query = DB::table('products')->insert([
        //     "nama" => $request["nama"],
        //     "stok" => $request["stok"],
        //     "deskripsi" => $request["deskripsi"],
        //     "image" => $request["image"],
        //     "harga" => $request["harga"],
        //     "categories_id" => $request["categories_id"],
        //     "transactions_id" => $request["transactions_id"],
        // ]);

        // Eloquent ORM
        $product = new Product;
        $product->nama = $request["nama"];
        $product->stok = $request["stok"];
        $product->deskripsi = $request["deskripsi"];
        $product->image = $request["image"];
        $product->harga = $request["harga"];
        $product->categories_id = $request["categories_id"];
        $product->transactions_id = $request["transactions_id"];
        $product->save();

        Alert::success('Berhasil', 'Berhasil Menambahkan Produk Baru');
        return redirect('/products')->with('success', 'Produk Berhasil Disimpan');

    }
    public function index(){
        // $products = DB::table('products')->get();
        // dd($cast);
        // Eloquent ORM
        $products = Product::all();
        return view('products.index', compact('products'));
    }

    public function show($id) {
        // $product = DB::table('products')->where('id', $id)->first();
        // dd($cast);
        // Eloquent ORM
        $product = Product::find($id);
        return view('products.show', compact('product'));
    }

    public function edit($id) {
        // $product = DB::table('products')->where('id', $id)->first();
        // Eloquent ORM
        $product = Product::find($id);

        return view('products.edit', compact('product'));
    }

    public function update($id, Request $request) {
        // $request->validate([
        //     'nama' => 'required|unique:products',
        //     'stok' => 'required',
        //     'deskripsi' => 'required',
        //     'image' => 'required',
        //     'harga' => 'required'
        // ]);

        // $query = DB::table('products')
        //             ->where('id', $id)
        //             ->update([
        //                 "nama" => $request["nama"],
        //                 "stok" => $request["stok"],
        //                 "deskripsi" => $request["deskripsi"],
        //                 "image" => $request["image"],
        //                 "harga" => $request["harga"],
        //                 "categories_id" => $request["categories_id"],
        //                 "transactions_id" => $request["transactions_id"],
        //             ]);

        // Model Eloquent
        $update = Product::where('id', $id)->update([
            "nama" => $request["nama"],
            "stok" => $request["stok"],
            "deskripsi" => $request["deskripsi"],
            "image" => $request["image"],
            "harga" => $request["harga"],
            "categories_id" => $request["categories_id"],
            "transactions_id" => $request["transactions_id"],
        ]);
        return redirect('/products')->with('success', 'Berhasil update produk');
    }
    public function destroy($id) {
        // $query = DB::table('products')->where('id', $id)->delete();
        // Model Eloquent
        Product::destroy($id);
        return redirect('/products')->with('success', 'Produk berhasil dihapus');
    }
    public function export() 
    {
        return Excel::download(new UsersExport, 'products.xlsx');
    }
}
