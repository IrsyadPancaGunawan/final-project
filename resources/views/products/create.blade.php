@extends('template.main')

@push('script-head')
  <script src="/path-to-your-tinymce/tinymce.min.js"></script>
@endpush

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Create New Products</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/products" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="string" class="form-control" id="nama" name="nama" placeholder="Nama Produk">
              @error('nama')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="stok">Stok</label>
                <input type="integer" class="form-control" id="stok" name="stok" placeholder="Stok">
                @error('stok')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">Deskripsi</label>
                <input type="longText" class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi">
                <textarea name="deskripsi" class="form-control my-editor">{!! old('deskripsi', $product ?? '') !!}</textarea>
                @error('deskripsi')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="image">Image</label>
              <input type="file" class="form-control" id="image" name="image" placeholder="Image">
              @error('image')
                <div class="alert alert-danger">{{ $message}}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="harga">Harga</label>
            <input type="integer" class="form-control" id="harga" name="harga" placeholder="Harga">
            @error('harga')
              <div class="alert alert-danger">{{ $message}}</div>
          @enderror
        </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </form>
      </div>
</div>

@push('script')
  <script src="https://cdn.tiny.cloud/1/x02intd8q747kahkmw5icm3gqutqn70a5o51nmldtw0kk7k0/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'string',
      plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush
@endsection

@push('script')
<script>
  var editor_config = {
    path_absolute : "/",
    selector: 'textarea.my-editor',
    relative_urls: false,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table directionality",
      "emoticons template paste textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    file_picker_callback : function(callback, value, meta) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
      if (meta.filetype == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.openUrl({
        url : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no",
        onMessage: (api, message) => {
          callback(message.content);
        }
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endpush
