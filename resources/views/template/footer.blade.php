<!-- footer -->
<div class="container-fluid">
    <div class="footer text-center">
        <p>Copyright © 2022 Designed by NIA Store. All rights reserved.<br><br>
            Distributed By: <a href="https://themewagon.com/">ThemeWagon</a>
        </p>
    </div>
</div>
</div>
<!-- end dashboard inner -->
</div>
</div>
</div>
<!-- jQuery -->
<script src="{{ asset('template/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('template/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('template/assets/js/bootstrap.min.js') }}"></script>
<!-- wow animation -->
<script src="{{ asset('template/assets/js/animate.js') }}"></script>
<!-- select country -->
<script src="{{ asset('template/assets/js/bootstrap-select.js') }}"></script>
<!-- owl carousel -->
<script src="{{ asset('template/assets/js/owl.carousel.js') }}"></script>
<!-- chart js -->
<script src="{{ asset('template/assets/js/Chart.min.js') }}"></script>
<script src="{{ asset('template/assets/js/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('template/assets/js/utils.js') }}"></script>
<script src="{{ asset('template/assets/js/analyser.js') }}"></script>
<!-- nice scrollbar -->
<script src="{{ asset('template/assets/js/perfect-scrollbar.min.js') }}"></script>
<script>
    var ps = new PerfectScrollbar('#sidebar');
</script>
<!-- custom js -->
<script src="{{ asset('template/assets/js/custom.js') }}"></script>
<script src="{{ asset('template/assets/js/chart_custom_style1.js') }}"></script>
