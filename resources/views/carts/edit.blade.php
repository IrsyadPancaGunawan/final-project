@extends('template.main')

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Edit Carts {{$cart->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/carts/{{$cart->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="string" class="form-control" id="nama" name="nama" placeholder="Nama Produk" value="{{ old('nama', $cart->nama)}}">
                  @error('nama')
                      <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                </div>
                <div class="form-group">
                    <label for="jumlah">jumlah</label>
                    <input type="string" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" value="{{ old('jumlah', $cart->jumlah)}}">
                    @error('jumlah')
                      <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="total_pembayaran">Total Pembayaran</label>
                  <input type="string" class="form-control" id="total_pembayaran" name="total_pembayaran" placeholder="Total Pembayaran" value="{{ old('total_pembayaran', $cart->total_pembayaran)}}">
                  @error('total_pembayaran')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
              </div>
              </div>
              <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
</div>

@endsection