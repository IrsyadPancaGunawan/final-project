@extends('template.main')

@push('script-head')
  <script src="/path-to-your-tinymce/tinymce.min.js"></script>
@endpush

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Create New Cart</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/carts" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="string" class="form-control" id="nama" name="nama" placeholder="Nama Produk">
              @error('nama')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="jumlah">Jumlah</label>
                <input type="integer" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah Produk">
                @error('jumlah')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
          </div>
          <div class="form-group">
            <label for="total_pembayaran">total_pembayaran</label>
            <input type="integer" class="form-control" id="total_pembayaran" name="total_pembayaran" placeholder="Total Pembayaran">
            @error('total_pembayaran')
              <div class="alert alert-danger">{{ $message}}</div>
          @enderror
      </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </form>
      </div>
</div>

@push('script')
  <script src="https://cdn.tiny.cloud/1/x02intd8q747kahkmw5icm3gqutqn70a5o51nmldtw0kk7k0/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'string',
      plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush
@endsection